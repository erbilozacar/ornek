"use strict";
process.chdir(__dirname);
const bot = require("./bot");

bot.catch(err => {
  console.log("Error: ", err);
});

bot.start(async ctx => {
  if (!ctx.chat.type.endsWith("private")) return null;
  ctx.reply("Merhaba");
});


bot.command("gs", async ctx => {
    if (!ctx.chat.type.endsWith("group")) return null;
    const chatMember = await ctx.telegram.getChatMember(ctx.message.chat.id, ctx.message.from.id);
    if (chatMember && [ 'creator', 'administrator' ].includes(chatMember.status)) {
        try {
            await ctx.telegram.sendPhoto(ctx.chat.id, "https://r.resimlink.com/fbd0r.jpg", {
                reply_markup: {
                    inline_keyboard: [
                        [
                            {
                                text: "VOLE SAYFAMIZ",
                                url: "https://vole.io/sadecegala"
                            }
                        ],
                        [
                            {
                                text: "TWİTTER SAYFAMIZ",
                                url: "https://twitter.com/sdcgalatasarayy"
                            }
                        ],
                        [
                            {
                                text: "İNSTAGRAM SAYFAMIZ",
                                url: "https://www.instagram.com/sdcgalatasaray/"
                            }
                        ],
                        [
                            {
                                text: "HABER KANALIMIZ",
                                url: "https://t.me/sadecegalatasarayhaber"
                            }
                        ],
                        [
                            {
                                text: "UYGULAMAMIZ",
                                url: "https://www.appcreator24.com/app920153"
                            }
                        ]
                    ]
                }
            });
        } catch (err) {
            console.log(err);
        }
    }
    ctx.deleteMessage()
});

bot.launch();
